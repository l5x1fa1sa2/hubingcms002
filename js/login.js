//获取元素

const button = document.querySelector('button')
const form = document.querySelector('form')
// const myToast = new bootstrap.Toast(document.querySelector("#myToast"), {
//     delay: 2000, // 显示多久后 自动消失
// });
button.addEventListener('click', async function (ev) {
    //ev.defaultPrevented()

    //传入表单dom元素， 返回表单数据对象
    const data = serialize(form, { hash: true })// hash意思是 设置返回的data是对象类型
    console.log(data.username);

    if (!data.username || !data.password) {
        document.querySelector('#myToast .toast-body').innerHTML = "账号或密码不能为空"
        myToast.show()
        return
    }
    const result = await axios({
        url: "/login",
        method: "post",
        data

    })
    console.log(result);


    if (result.request.status === 200) {
        myToast.show();

        document.querySelector('#myToast .toast-body').innerHTML = "登陆成功"
        // 登录成功后将返回的token数据保存到本地存储中
        localStorage.setItem('token8887', result.data.data.token);
        // 创建延时器，延时跳转至登录页
        setTimeout(() => {
            // 跳转至登录页
            location.href = 'index.html';
        }, 2000);
    }


})

