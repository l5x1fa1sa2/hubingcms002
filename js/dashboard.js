// 获取元素
const btns = document.querySelector('#btns');

//发送请求 获取学员信息
async function getData() {
    const result = await axios({
        url: "/dashboard"
    })
    console.log(result.data);
    const { year, groupData, overview, provinceData, salaryData } = result.data.data
    console.log(groupData);
    //console.log(salaryData);
    //
    const month = year.map((v) => v.month);
    const salary = year.map((v) => v.salary)


    let salaryArr = [];
    salaryData.forEach(item => {
        salaryArr.push({ value: item.g_count + item.b_count, name: item.label })
    })
    //console.log(salary, label, gCount, bCount, value, name);

    //console.log(month, salary);
    //薪资走势图
    function dataList(year) {

        const myChart = echarts.init(document.querySelector("#line"));

        // 指定图表的配置项和数据
        const option = {
            title: {
                text: '2021全学科薪资走势'
            },
            color: "#698cf7",
            legend: {
                data: ['2021全学科薪资走势']
            },

            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            yAxis: {
                type: "value",
                axisLabel: {
                    formatter: "{value}"
                }
            },
            xAxis: {
                type: "category",
                axisLine: { onZero: false },
                axisLabel: {
                    formatter: "{value}"
                },
                boundaryGap: false,
                data: month
            },
            series: [
                {
                    name: '日期',
                    type: 'line',
                    symbolSize: 10,
                    //type: 'category',
                    smooth: true,
                    //symbol: "circle",
                    lineStyle: {
                        width: 8,
                        shadowColor: 'rgba(0,0,0,0.3)',
                        shadowBlur: 10,
                        shadowOffsetY: 8
                    },
                    data: salary
                }
            ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);


    }
    dataList()

    //薪资分布图


    function salaryPieEchart(salaryData) {
        // 基于准备好的dom，初始化echarts实例
        const myChart = echarts.init(document.querySelector('#salary'))

        // 指定图表的配置项和数据
        const option = {
            title: {
                text: '班级薪资分布',
                top: '2%',
                left: '3%'
            },
            tooltip: {
                trigger: 'item'
            },
            // 图例
            legend: {
                bottom: '5%',
                left: 'center'
            },
            // 颜色
            color: ['#fda224', '#5097ff', '#3fceff', '#34d39a', 'yellowgreen'],
            series: [
                {
                    name: 'Access From',
                    type: 'pie',
                    radius: ['60%', '80%'],
                    avoidLabelOverlap: false,
                    itemStyle: {
                        borderRadius: 10,
                        borderColor: '#fff',
                        borderWidth: 2
                    },
                    label: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '40',
                            fontWeight: 'bold'
                        }
                    },
                    labelLine: {
                        show: false
                    },
                    data: salaryArr

                }
            ]
        }
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option)
    }
    salaryPieEchart(salaryData)


    //班级每组薪资
    function groupSalaryEchart(groupData) {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.querySelector('#lines'))

        // 指定图表的配置项和数据
        const option = {
            // x轴设置
            xAxis: {
                type: 'category',
                data: groupData[1].map(v => v.name),
                // 线段设置
                axisLine: {
                    // 线的样式
                    lineStyle: {
                        type: 'dashed',
                        color: '#d4d4d6'
                    }
                }
            },
            yAxis: {
                type: 'value',
                // 分割线设置
                splitLine: {
                    // 线的样式
                    lineStyle: {
                        type: 'dashed',
                        color: '#d4d4d6'
                    }
                }
            },
            // 颜色
            color: [
                // 渐变
                {
                    type: 'linear',
                    x: 0,
                    y: 0,
                    x2: 0,
                    y2: 1,
                    colorStops: [
                        {
                            offset: 0,
                            color: '#34d29a' // 0% 处的颜色
                        },
                        {
                            offset: 1,
                            color: '#d4f5ea' // 100% 处的颜色
                        }
                    ],
                    global: false // 缺省为 false
                },
                {
                    type: 'linear',
                    x: 0,
                    y: 0,
                    x2: 0,
                    y2: 1,
                    colorStops: [
                        {
                            offset: 0,
                            color: '#499fed' // 0% 处的颜色
                        },
                        {
                            offset: 1,
                            color: '#d8eafb' // 100% 处的颜色
                        }
                    ],
                    global: false // 缺省为 false
                }
            ],
            series: [
                // 柱状图 给多个图形,会有柱形
                {
                    data: groupData[1].map(v => v.hope_salary),
                    type: 'bar'
                },
                {
                    data: groupData[1].map(v => v.salary),
                    type: 'bar'
                }
            ]
        }

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option)

        // ---------- 点击按钮高亮 ----------
        // 切换组号 显示班级 不同组号的 薪资
        // 事件委托 给班级组号div  绑定点击事件
        btns.addEventListener('click', function (e) {
            if (e.target.tagName === "BUTTON") {
                // 点击高亮  移除以前标签的 类 btn-blue
                document.querySelector('.btn-blue') ? document.querySelector('.btn-blue').classList.remove('btn-blue') : "";
                // // 给当前被点击的标签 添加类 btn-blue
                e.target.classList.add('btn-blue');
                // 获取组号
                const groupNumber = e.target.innerText
                // 班级每组薪资图
                groupSalaryEchart(groupData, groupNumber);
            }
        });




    }
    groupSalaryEchart(groupData)

    //男女
    function boyGirlEchart(salaryData) {
        console.log('groupData:', groupData)
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.querySelector('#gender'))

        // 指定图表的配置项和数据
        const option = {
            // title可以设置一个数组 给多个标题
            title: [
                {
                    text: '男女薪资分布',
                    top: '3%',
                    left: '4%'
                },
                {
                    text: '男生',
                    left: 'center',
                    top: '45%'
                },
                {
                    text: '女生',
                    left: 'center',
                    top: '85%'
                }
            ],
            tooltip: {
                trigger: 'item'
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                show: false
            },
            color: ['#fda224', '#5097ff', '#3abcfa', '#34d39a', 'yellowgreen'],
            series: [
                {
                    name: 'Access From',
                    type: 'pie',
                    radius: ['25%', '35%'],
                    // 圆心的位置
                    center: ['50%', '30%'],
                    data: salaryArr
                    ,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                },
                {
                    name: 'Access From',
                    type: 'pie',
                    radius: ['25%', '35%'],
                    center: ['50%', '70%'],
                    data: salaryArr,

                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        }

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option)
    }
    boyGirlEchart(salaryData)
}
getData()



