const myToast = new bootstrap.Toast(document.querySelector("#myToast"), {
    delay: 2000, // 显示多久后 自动消失
});

function bsShow(msg) {
    myToast.show()
    document.querySelector('#myToast .toast-body').innerHTML = msg
}



axios.defaults.baseURL = 'http://ajax-api.itheima.net';

// //添加拦截器
// axios.interceptors.request.use(function (config) {
//     // 在发送请求之前做些什么

//     return config;
// }, function (error) {
//     // 对请求错误做些什么
//     return Promise.reject(error);
// });

// // 添加响应拦截器
// axios.interceptors.response.use(function (response) {
//     // 2xx 范围内的状态码都会触发该函数。
//     // 对响应数据做点什么
//     //判断业务状态码 如果 不等于0，也弹出窗口
//     /* if (response.data.code !== 0) {
//         toastr["info"](response.data.message, "提示")
//     }
//     NProgress.done() */
//     console.log(状态正常);
//     return response;
// }, function (error) {
//     // 超出 2xx 范围的状态码都会触发该函数。
//     // 对响应错误做点什么
//     //toastr["error"](error.response.data.detil[0], "注意")
//     bsShow(error.response.data.detil[0])
//     return Promise.reject(error);
// });
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    // 在发送请求之前做些什么
    if (localStorage.getItem("token8887")) {
        config.headers.Authorization = localStorage.getItem("token8887")
    }
    //NProgress.start()
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    console.log("响应拦截器 1 http状态码正常");
    return response;
}, function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    console.log("响应拦截器 2 http状态码错误");
    console.dir(error);
    //bsShow(error.response.data.detail[0].message)
    return Promise.reject(error);
});

//获取退出元素
const logout = document.querySelector("#logout")


//退出点击事件
logout.addEventListener('click', function (e) {
    if (confirm("您确定退出么？")) {
        //清空本地存储
        localStorage.removeItem("token8887")
        //跳转回登陆页面
        location.href = "login.html"
    }
})
