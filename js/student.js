const tbody = document.querySelector('tbody')
const biPen = document.querySelector('#bi-pen')
const addModal = document.querySelector('#openModal');
const btnWhite = document.querySelector('#btn-white')

//获取dom元素
const provinceEle = document.querySelector('select[name="province"]')
const cityEle = document.querySelector('select[name="city"]')
const areaEle = document.querySelector('select[name="area"]')
//const nameEle = document.querySelector('input[name="name"]')
//const btnBlue = document.querySelector('.btn-blue')
const form = document.querySelector('form');
//const submitBtn = document.querySelector("#submit");
//页面渲染函数
let id;
async function getScore() {
    //定义一个函数
    const result = await axios({
        url: "/students",
        //请求头中携带token
        Headers: {
            Authorzation: localStorage.getItem("token8887")
        }
    })
    //console.log(result);
    let html = ""
    for (const key in result.data.data) {
        //console.log(key);
        //console.log(result.data.data[key]);
        const { id, name, age, group, salary, gender, hope_salary, province, city, area } = result.data.data[key]
        let sex = gender === 0 ? '男' : '女';
        html += `<tr>
        <th>${name}</th>
        <th>${age}</th>
        <th>${sex}</th>
        <th>${group}</th>
        <th>${hope_salary}</th>
        <th>${salary}</th>
        <th>${province}${city}${area}</th>
        <th><a href="javascript:;" class="text-success mr-3"><i data-id="${id}" class="bi bi-pen"></i></a>
        <a href="javascript:;" class="text-danger"><i data-id="${id}" class="bi bi-trash"></i></a>
      </th>
      </tr>`
    }
    tbody.innerHTML = html
}
getScore()

//初始化省下拉列表
async function fetchProvince() {
    getSelect("/api/province", {}, "省", provinceEle)
}
//封装统一获取下拉列表数据和渲染组件
async function getSelect(url, params, text, target) {
    const result = await axios({
        url,
        params
    })
    const { data } = result.data; // 地区数据 数组

    target.innerHTML = data.reduce((beforeHTML, item) => {
        return beforeHTML + `<option value="${item}">--${item}--</option>`;
    }, `<option value="">--${text}--</option>`);
};

// 初始化下拉 省份 组件



getScore()
fetchProvince()
//////
/* const fetchProvince = async () => {
    const res = await axios({
        url: '/api/province'
    })
    const { data } = res.data
    //console.log(1, res.data);
    provinceEle.innerHTML = document.querySelector('select[name="province"]').innerHTML = data.map((name) => {
        return `<option value="${name}">${name}</option>`
    }).join('')
}
fetchProvince() */
//初始化省下拉列表
provinceEle.addEventListener('change', async function fetchCity() {

    getSelect("api/city", { pname: provinceEle.value }, "市", cityEle)

})
//
cityEle.addEventListener('change', async function fetchArea() {

    getSelect("/api/area", { pname: provinceEle.value, cname: cityEle.value }, "县", areaEle)

})
//创建一个新增的模态框实例
const modal = new bootstrap.Modal(document.querySelector('#modal'));

//给确认按钮添加点击事件
// form 绑定提交事件
form.addEventListener("submit", async function (e) {
    e.preventDefault();
    // 快速获取表单的数据
    const data = serialize(form, { hash: true, empty: true });

    const id = data.id;
    delete data.id;

    // 校验字段合法性
    for (const key in data) {
        if (!data[key]) {
            bsShow("您有字段还未处理");
            return;
        }
    }
    data.age = +data.age;
    data.gender = +data.gender;
    data.salary = +data.salary;
    data.hope_salary = +data.hope_salary;
    data.group = +data.group;
    // submitBtn.addEventListener("click", async function () {
    //     //判断标题
    //     const data = serialize(form, { hash: true, empty: true });
    const modalTitle = document.querySelector('.modal-title').innerText
    let result
    if (modalTitle === "添加新学员") {
        result = await axios({
            url: "/students",
            method: 'post',
            data

        })
    } else if (modalTitle === "修改学员信息") {
        data.id = id
        result = await axios({
            url: `/students/${id}`,
            method: "put",
            data
        })
    }


    // 重新渲染数据


    // 关闭模态框、
    bsShow("操作成功")
    //console.log(result);

    modal.hide();//关闭模态框
    form.reset()
    await getScore();
    cityEle.innerHTML = cityEle.firstChild.outerHTML;
    areaEle.innerHTML = areaEle.firstChild.outerHTML;
});






tbody.addEventListener("click", async function (ev) {
    //console.log(ev.target.classList.contains('bi-pen'));
    const target = ev.target
    if (target.classList.contains('bi-pen')) {
        modal.show()
        document.querySelector('.modal-title').innerHTML = "修改学员信息"
        id = +target.dataset.id
        console.log(id);

        const result = await axios({
            url: `/students/${id}`,
            method: "get",
            params: {
                id
            }
        })
        const obj = result.data.data
        await getSelect("api/city", { pname: obj.province }, "市", cityEle)


        await getSelect("api/area", { pname: obj.province, cname: obj.city }, "县", areaEle)

        for (const key in obj) {
            const formEle = document.querySelector(`[name=${key}]`)
            if (formEle && key !== "gender") {
                formEle.value = obj[key]
            }

        }
        document.querySelector(`input[name=gender][value="${obj.gender}"]`).checked = true
    }
    //删除功能
    if (ev.target.classList.contains('bi-trash')) {
        if (confirm("您确定删除么？")) {
            const id = ev.target.dataset.id
            const result = await axios({
                url: `/students/${id}`,
                method: 'delete'
            })
            getScore()
        }

    }
    // 获取省份数据



})
addModal.addEventListener("click", function (e) {
    modal.show()
    document.querySelector(".modal-title").innerText = "添加新学员";
})