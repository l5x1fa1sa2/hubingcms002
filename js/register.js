const form = document.querySelector('form')


form.addEventListener('submit', async function (e) {
    e.preventDefault()

    const username = form.querySelector('input[name=username]').value.trim()
    const password = form.querySelector('input[name=password]').value.trim()

    if (!username || !password) {
        alert('输入不合法，请重新输入')
        return
    }

    const result = await axios({
        url: "/register",
        method: "post",
        data: {
            username,
            password
        }
    })
    console.log(result);
    console.log(result.data.message);
    bsShow(result.data.message);

    //document.querySelector('#myToast .toast-body').innerHTML = "登陆成功"
    // 登录成功后将返回的token数据保存到本地存储中
    //localStorage.setItem('token8887', result.data.data.token);
    // 创建延时器，延时跳转至登录页
    // setTimeout(() => {
    //     // 跳转至登录页
    //     location.href = 'login.html';
    // }, 2000);
})